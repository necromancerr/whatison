//
//  WhatIsOnTests.swift
//  WhatIsOnTests
//
//  Created by Pratish Karmacharya on 5/1/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import XCTest
import Moya

@testable import WhatIsOn

class WhatIsOnTests: XCTestCase {
    var networkManager: NetworkManager!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        networkManager = NetworkManager(provider: MoyaProvider<NetworkAPI>(stubClosure: MoyaProvider.immediatelyStub))
    }
    
    
    func testGetSchedule() {
        let country = "US"
        let date = "2019-05-02"
        
        let expectation = self.expectation(description: "testing schedule")
        networkManager.getSchedules(country: country, date: date) { (results, error) in
            if let programs = results {
                assert(programs.count == 1, "Results Count mismatch")
                assert(programs[0].name == "The Robot", "Name didn't match")
                assert(programs[0].show?.showImage?.medium == nil)
            }
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }

    func testProgramListTableViewController() {
        let controller = ProgramListTableViewController(nibName: "ProgramListTableView", bundle: nil)
        controller.networkManager = networkManager
        // Load the controller view otherwise UI elements won't be initialized
        controller.loadViewIfNeeded()
        
        XCTAssertNotNil(controller.filterModel, "Filtermodel not initialized")
        XCTAssertEqual(controller.filterModel.country, "US", "Invalid country detected in filter model")
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = Date()
        let dateString = formatter.string(from: date)
        XCTAssertEqual(controller.filterModel.date, dateString, "Failed to load today's date in filter model")
        
        XCTAssertNotNil(controller.programTableView, "ProgramListTableView doesn't have a tableview")
        XCTAssertNotNil(controller.programs, "Programs is empty")
        XCTAssertNotNil(controller.networkManager, "Network manager is not initialized")
        
        XCTAssertTrue(controller.programTableView.dataSource is ProgramListTableViewController, "TableView's datasource is not self")
        XCTAssertTrue(controller.programTableView.delegate is ProgramListTableViewController, "TableView's delegate is not self")
        
        XCTAssertEqual(controller.programs.count, 1, "Sample data loaded incorrect program count")
        XCTAssertEqual(controller.tableView(controller.programTableView, numberOfRowsInSection: 0), 1, "Tableview has more than one table")
        
        XCTAssertEqual(controller.programs[0].name, "The Robot" , "Name didn't match")
        XCTAssertNil(controller.programs[0].show?.showImage?.medium, "Medium image shouldn't be present")
        
        XCTAssertTrue(controller.tableView(controller.programTableView, cellForRowAt: IndexPath(row: 0, section: 0)) is ProgramTableViewCell, "ProgramTableView dequeued invalid cell")
        
        XCTAssertTrue(controller is ProgramFilterViewControllerDelegate, "ProgramListViewController is no ProgramFilterViewController's delegate")
    }
    
    func testProgramFilterView() {
        let controller = ProgramListTableViewController(nibName: "ProgramListTableView", bundle: nil)
        controller.networkManager = networkManager
        // Load the controller view otherwise UI elements won't be initialized
        controller.loadViewIfNeeded()
        
        let filterView = ProgramFilterViewController(nibName: "ProgramFilterView", bundle: nil)
        filterView.loadViewIfNeeded()
        
        let rightNavButton = controller.navigationItem.rightBarButtonItem
        XCTAssertNotNil(rightNavButton, "Filter view button not created")
        
        XCTAssertNil(filterView.delegate, "FilterView should not have a delegate at this sgate")
        XCTAssertNil(filterView.filterModel, "Filter Model is not nil. It should be at this stage")
        filterView.delegate = controller
        filterView.filterModel = controller.filterModel
        
        XCTAssertNotNil(filterView.filterModel, "Filter Model should no longer be nil")
        XCTAssertNotNil(filterView.delegate, "FilterView should have a delegate now")
        
        XCTAssertEqual(filterView.filterModel.country, controller.filterModel.country, "Two filtermodel should have same value for country")
        XCTAssertEqual(filterView.filterModel.date, controller.filterModel.date, "Two filtermodel should have same value for date")
        
        // Change values
        filterView.filterModel.date = "2019-05-01"
        filterView.filterModel.country = "GB"
        
        XCTAssertNotEqual(filterView.filterModel.date, controller.filterModel.date, "Two filtermodel should not have same  value for date")
        XCTAssertNotEqual(filterView.filterModel.country, controller.filterModel.country, "Two filtermodel should not have same value for country")
        
        // Apply delegate
        filterView.delegate?.applyFilter(filterView.filterModel)
        
        XCTAssertEqual(filterView.filterModel.country, controller.filterModel.country, "Two filtermodel should have same value for country")
        XCTAssertEqual(filterView.filterModel.date, controller.filterModel.date, "Two filtermodel should have same value for date")
    }
    
    func testDetailViewController() {
        let detailView = ProgramDetailViewController(nibName: "ProgramDetailView", bundle:nil)
        detailView.loadViewIfNeeded()
        
        let controller = ProgramListTableViewController(nibName: "ProgramListTableView", bundle: nil)
        controller.networkManager = networkManager
        controller.loadViewIfNeeded()
        controller.tableView(controller.programTableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        detailView.program = controller.programs[0]
        
        XCTAssertNotNil(detailView.program, "Program shouldn't be nill")
        XCTAssertEqual(detailView.program!, controller.programs[0], "Programs dont' match.")
        
        XCTAssertNotNil(detailView.imageView, "Image view didn't load")
        XCTAssertNotNil(detailView.showNameLabel, "ShowName label didn't load")
    }
    
    func testShowDetailViewController() {
        let controller = ProgramListTableViewController(nibName: "ProgramListTableView", bundle: nil)
        controller.networkManager = networkManager
        // Load the controller view otherwise UI elements won't be initialized
        controller.loadViewIfNeeded()
        
        let showDetail = ShowDetailViewController(nibName: "ShowDetailViewController", bundle: nil)
        showDetail.loadViewIfNeeded()
        
        XCTAssertNotNil(showDetail, "Failed to load ShowDetailViewController")
        
        showDetail.show = controller.programs[0].show
        showDetail.viewDidLoad()
        
        XCTAssertEqual(controller.programs[0].show?.name, showDetail.showNameLabel.text, "Show name didn't match")
        XCTAssertEqual(controller.programs[0].show?.officialSite, showDetail.officialSiteLabel.text, "Official site didn't match")
        XCTAssertNil(showDetail.showImage, "At this point, showImage should be nil")
        
        let programDetail = ProgramDetailViewController(nibName: "ProgramDetailView", bundle: nil)
        programDetail.program = controller.programs[0]
        programDetail.viewDidLoad()
        
        showDetail.showImage = programDetail.imageView.image
        showDetail.viewDidLoad()
        XCTAssertEqual(showDetail.imageView.image, programDetail.imageView.image, "Images's didn't match")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
