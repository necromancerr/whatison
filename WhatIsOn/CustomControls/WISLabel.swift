//
//  WIS.swift
//  WhatIsOn
//
//  Created by Pratish Karmacharya on 5/4/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import UIKit

class WISLabel: UILabel {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.initialize()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    private func initialize() {
        self.textColor = UIColor.WISColor.labelColor
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
