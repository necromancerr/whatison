//
//  WISTabBarViewController.swift
//  WhatIsOn
//
//  Created by Pratish Karmacharya on 5/4/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import UIKit

class WISTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addChild(ProgramListNavigationController())
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
