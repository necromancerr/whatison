//
//  ProgramFilterViewController.swift
//  WhatIsOn
//
//  Created by Pratish Karmacharya on 5/2/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import UIKit

protocol ProgramFilterViewControllerDelegate: AnyObject {
    func applyFilter(_ filterModel: FilterModel)
}

class ProgramFilterViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var dateTextField: UITextField!;
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var showNameTextField: UITextField!
    
    weak var delegate: ProgramFilterViewControllerDelegate?
    
    var filterModel:FilterModel!
    
    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        formatter.dateFormat = "yyyy-MM-dd"
        
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.addTarget(self,
                             action: #selector(theDatePickerValueChanged),
                             for: .valueChanged)
        dateTextField.inputView = datePicker
        
        let gestureRecognizer = UITapGestureRecognizer.init(target: self,
                                                            action: #selector(backgroundTap));
        view.addGestureRecognizer(gestureRecognizer)
        
        loadFilters()
    }
    
    private func loadFilters() {
        if filterModel != nil {
            dateTextField.text = filterModel.date
            countryTextField.text = filterModel.country
            showNameTextField.text = filterModel.showName
        }
    }
    
    // MARK: Gesture Recognizer
    @objc func backgroundTap(gesture : UITapGestureRecognizer) {
        dateTextField.resignFirstResponder() // or view.endEditing(true)
    }
    
    // MARK: TextField Delegate
    @objc func theDatePickerValueChanged(sender: UIDatePicker) {
        dateTextField.text = formatter.string(from: sender.date)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if dateTextField.text!.isEmpty {
            dateTextField.text = filterModel.date
        }
        return true
    }

    // MARK: Gesture recognizers
    @IBAction func applyTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        filterModel.date = dateTextField.text!
        filterModel.country = countryTextField.text!
        filterModel.showName = showNameTextField.text
        let showName = showNameTextField.text!;
        if showName.isEmpty {
            filterModel.showName = nil
        } else {
            filterModel.showName = showName
        }
        
        self.delegate?.applyFilter(filterModel)
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
