//
//  ShowDetailViewController.swift
//  WhatIsOn
//
//  Created by Pratish Karmacharya on 5/4/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import UIKit

class ShowDetailViewController: UIViewController {

    @IBOutlet weak var blurView: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var showNameLabel: UILabel!
    @IBOutlet weak var runtimeLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var premieredLabel: UILabel!
    @IBOutlet weak var officialSiteLabel:UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    
    var show:Show?
    var score: Float?
    var showImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        blurView.addSubview(blurEffectView)
        blurView.contentMode = .scaleToFill
        loadShow()
        
        // Do any additional setup after loading the view.
    }

    
    private func loadShow() {
        if let scoreText = score {
            scoreLabel.text = String.init(scoreText)
        }
        if let show = show {
            if let _ = self.showImage {
                self.imageView.image = self.showImage
                self.blurView.image = self.showImage
            } else {
                if let imageUrl = show.showImage?.original {
                    let url = URL(string: imageUrl)!
                    URLSession.shared.dataTask(with: url) { data, response, error in
                        guard
                            let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                            let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                            let data = data, error == nil,
                            let image = UIImage(data: data)
                            else { return }
                        DispatchQueue.main.async() {
                            self.imageView.image = image
                            self.blurView.image = image
                        }
                        }.resume()
                }
            }
            showNameLabel.text = show.name
            runtimeLabel.text = String.init(show.runtime!)
            typeLabel.text = show.type
            languageLabel.text = show.language
            statusLabel.text = show.status
            premieredLabel.text = show.premiered
            officialSiteLabel.text = show.officialSite
            if let genres = show.genres {
                if genres.count > 0 {
                    genresLabel.text = genres[0]
                }
            }
            summaryLabel.attributedText = show.summary?.htmlToAttributedString
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
