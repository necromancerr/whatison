//
//  ProgramDetailViewController.swift
//  WhatIsOn
//
//  Created by Pratish Karmacharya on 5/1/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import UIKit

class ProgramDetailViewController: UIViewController {



    var program: Program!
    @IBOutlet weak var imageView : UIImageView!
    @IBOutlet weak var seasonLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var runtimeLabel: UILabel!
    @IBOutlet weak var programNameLabel: UILabel!
    @IBOutlet weak var showNameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var premieredLabel: UILabel!
    @IBOutlet weak var airedLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var blurView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        blurView.addSubview(blurEffectView)
        blurView.contentMode = .scaleToFill
        loadProgram()
    }

    func loadProgram() {
        if let program = program {
            if let name = program.name {
                programNameLabel.text = name
            }
            if let season = program.season {
                seasonLabel.text = String.init(season)
            } else
            if let number = program.number {
                numberLabel.text = String.init(number)
            }
            if let runtime = program.runtime {
                runtimeLabel.text = String.init(runtime)
            }
            if let airstamp = program.airstamp {
                let dateFormatter = ISO8601DateFormatter()
                dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
                let date = dateFormatter.date(from: airstamp)
                let formatter = DateFormatter()
                formatter.dateFormat = "hh:mm a 'on' MM-dd-yyyy"
                formatter.amSymbol = "AM"
                formatter.pmSymbol = "PM"
                formatter.timeZone = TimeZone.current
                airedLabel.text = formatter.string(from: date!)
            }
            
            if let show = program.show {
                if let imageUrl = show.showImage?.medium {
                    let url = URL(string: imageUrl)!
                    URLSession.shared.dataTask(with: url) { data, response, error in
                        guard
                            let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                            let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                            let data = data, error == nil,
                            let image = UIImage(data: data)
                            else { return }
                        DispatchQueue.main.async() {
                            self.imageView.image = image
                            self.blurView.image = image
                        }
                        }.resume()
                }
                showNameLabel.text = show.name
                typeLabel.text = show.type
                languageLabel.text = show.language
                statusLabel.text = show.status
                premieredLabel.text = show.premiered

                if let summary = program.summary {
                    summaryLabel.attributedText = summary.htmlToAttributedString
                }
            }
        }
    }
    
    @IBAction func showMoreButtontapped(_ sender: Any) {
        let showDetailViewController = ShowDetailViewController(nibName: "ShowDetailViewController", bundle: nil)
        showDetailViewController.show = program.show
        showDetailViewController.showImage = self.imageView.image
        self.navigationController?.pushViewController(showDetailViewController, animated: true)
    }
}

