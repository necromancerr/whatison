//
//  ViewController.swift
//  WhatIsOn
//
//  Created by Pratish Karmacharya on 5/1/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import UIKit
import Moya

class ProgramListTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let viewControllerTitle = "Program List"
    let programCellId = "programCellId"
    let programTableViewCellHeight:CGFloat = 55.0
    
    var filterModel:FilterModel!
    
    @IBOutlet weak var programTableView:UITableView!;
    
    var networkManager:NetworkManager = NetworkManager()
    
    var programs:[Program] = []
    var shows:[SearchShowResult] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = viewControllerTitle
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        filterModel = FilterModel(date: formatter.string(from: date), country: "US")
        
        self.programTableView.register(ProgramTableViewCell.self, forCellReuseIdentifier: programCellId)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchTapped))
        
        setupEmptyView()
        
        refreshData()
    }
    
    // Setup empty view for the tableview when there's no data.
    private func setupEmptyView() {
        let backgroundView = UIView(frame: self.programTableView.bounds)
        let catImage = UIImage.init(named: "cat")
        let imageView = UIImageView(frame: backgroundView.bounds)
        imageView.contentMode = .scaleAspectFill
        imageView.image = catImage
        backgroundView.addSubview(imageView)
        
        let backgroundLabel = UILabel(frame: backgroundView.bounds)
        backgroundLabel.numberOfLines = 2
        backgroundLabel.text = "Program list is empty. \nTry changing your filters."
        backgroundLabel.backgroundColor = UIColor.clear
        backgroundLabel.textAlignment = .center
        backgroundView.addSubview(backgroundLabel)
        
        self.programTableView.backgroundView = backgroundView
    }
    
    // Get data from server.
    private func refreshData() {
        self.programs = []
        // uncomment this line to test against sample stub.
        //networkManager = NetworkManager(provider: MoyaProvider<NetworkAPI>(stubClosure: MoyaProvider.immediatelyStub))
        if let showName = filterModel.showName {
            self.networkManager.getSearchShow(showName: showName, completion: { (results, error) in
                if let err = error {
                    print(err)
                }
                if let response = results {
                    self.programs = response
                }
                self.programTableView.reloadData()
            })
        } else {
            self.networkManager.getSchedules(country: filterModel.country, date: filterModel.date, completion: { (results, error) in
                if let err = error {
                    print(err)
                }
                if let response = results {
                    self.programs = response
                }
                self.programTableView.reloadData()
            })
        }
        
    }
    
    // MARK: Gesture recognizer methods
    @objc func searchTapped(sender: UIBarButtonItem) {
        let filterView = ProgramFilterViewController(nibName: "ProgramFilterView", bundle: nil)
        filterView.filterModel = self.filterModel
        filterView.delegate = self
        filterView.modalPresentationStyle = .overCurrentContext
        self.present(filterView, animated: true, completion: nil)
    }
    
    // MARK: TableView Delegate and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if programs.count == 0 {
            tableView.separatorStyle = .none
            tableView.backgroundView?.isHidden = false
        } else {
            tableView.separatorStyle = .singleLine
            tableView.backgroundView?.isHidden = true
        }
        
        return programs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("ProgramTableViewCell", owner: self, options: nil)?.first as! ProgramTableViewCell
        
        if let show = programs[indexPath.row].show {
            cell.programNameLabel.text = show.name
            cell.channelLabel.text = show.network?.name
            if let imagePath = show.showImage?.medium {
                cell.programImageView.downloadFromServer(link: imagePath, contentMode: .scaleAspectFill)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return programTableViewCellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let _ = filterModel.showName {
            let detailView = ShowDetailViewController(nibName: "ShowDetailViewController", bundle: nil)
            detailView.show = programs[indexPath.row].show
            detailView.score = programs[indexPath.row].score
            self.navigationController?.pushViewController(detailView, animated: true)
        } else {
            let detailView = ProgramDetailViewController(nibName: "ProgramDetailView", bundle: nil)
            detailView.program = programs[indexPath.row];
            self.navigationController?.pushViewController(detailView, animated: true)
        }
    }
}

extension ProgramListTableViewController: ProgramFilterViewControllerDelegate {
    func applyFilter(_ filterModel: FilterModel) {
        self.filterModel = filterModel
        refreshData()
    }
}
