//
//  WISNavigationController.swift
//  WhatIsOn
//
//  Created by Pratish Karmacharya on 5/4/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import UIKit

class ProgramListNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let programListViewController = ProgramListTableViewController(nibName: "ProgramListTableView", bundle: nil)
        self.pushViewController(programListViewController, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
