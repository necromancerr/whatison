//
//  ProgramTableViewCell.swift
//  WhatIsOn
//
//  Created by Pratish Karmacharya on 5/1/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import UIKit

class ProgramTableViewCell: UITableViewCell {

    @IBOutlet weak var programNameLabel:UILabel!;
    @IBOutlet weak var programImageView:UIImageView!;
    @IBOutlet weak var channelLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        programImageView.layer.borderWidth = 1
        programImageView.layer.masksToBounds = false
        programImageView.layer.borderColor = UIColor.black.cgColor
        programImageView.layer.cornerRadius = programImageView.frame.height/2
        programImageView.clipsToBounds = true
        
        channelLabel.textColor = UIColor.WISColor.secondaryColor
    }
}
