//
//  NetworkManager.swift
//  WhatIsOn
//
//  Created by Pratish Karmacharya on 5/1/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import Foundation
import Moya
import Alamofire


struct NetworkManager {
    
    let provider: MoyaProvider<NetworkAPI>
    
    init(provider: MoyaProvider<NetworkAPI> =  MoyaProvider<NetworkAPI>(plugins: [NetworkLoggerPlugin(verbose: true)])) {
        self.provider = provider
    }
    
    /**
    The schedule is a complete list of episodes that air in a given country on a given date. Episodes are returned in the order in which they are aired, and full information about the episode and the corresponding show is included.
     
     - parameter country: ISO 3166-1 country code.
     - parameter date: ISO 8601 formated date
     - parameter completion: list of [Program] and Error is returned.
    */
    func getSchedules(country: String, date: String, completion: @escaping ([Program]?, Error?)->Void){
        provider.request(.schedule(country: country, date: date)) { result in
            switch result {
            case let .success(response):
                do {
                    let results = try JSONDecoder().decode([Program].self, from: response.data)
                    completion(results, nil)
                } catch let err {
                    completion(nil, err)
                }
            case let .failure(error):
                completion(nil, error)
            }
        }
    }
    
    /**
     Search through all the shows in our database by the show's name. A fuzzy algorithm is used (with a fuzziness value of 2), meaning that shows will be found even if your query contains small typos. Results are returned in order of relevancy (best matches on top) and contain each show's full information.
     
     - parameter showName: Name of the show to search
     - parameter completion: Returns list of [Program] and Error.
    */
    func getSearchShow(showName: String, completion: @escaping ([Program]?, Error?) -> ()) {
        provider.request(.searchShow(showName: showName)) { result in
            switch result {
            case let .success(response):
                do {
                    let results = try JSONDecoder().decode([Program].self, from: response.data)
                    completion(results, nil)
                } catch let err {
                    completion(nil, err)
                }
            case let .failure(error):
                completion(nil, error)
            }
        }
    }
}

