//
//  StringExtension.swift
//  WhatIsOn
//
//  Created by Pratish Karmacharya on 5/3/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import UIKit

/**
 Adds ability for String with html tags to an attributedstring.
 */
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
