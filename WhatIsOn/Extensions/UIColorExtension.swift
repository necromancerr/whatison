//
//  WISColor.swift
//  WhatIsOn
//
//  Created by Pratish Karmacharya on 5/4/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import Foundation
import UIKit

/**
 Add custom colors to the UIColor for easy access
 */
extension UIColor {
    struct WISColor {
        
        static let mainColor = UIColor.orange
        
        static let secondaryColor = UIColor.lightGray
        
        static let labelColor = UIColor.red
    }
}
