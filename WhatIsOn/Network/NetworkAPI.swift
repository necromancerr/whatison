//
//  NetworkAPI.swift
//  WhatIsOn
//
//  Created by Pratish Karmacharya on 5/1/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import Foundation
import Moya

enum NetworkAPI {
    case schedule(country: String, date: String)
    case searchShow(showName: String)
}


extension NetworkAPI : TargetType {
    // Base URL for the api
    var baseURL : URL {
        guard let url = URL(string: "http://api.tvmaze.com/") else { fatalError("Base url not found")}
        return url
    }
    
    /**
     Defines all path that the api supports
     */
    var path: String {
        switch self {
        case .schedule:
            return "schedule"
        case .searchShow:
            return "search/shows"
        }
        
    }
    
    /**
    Defines HTTPMethod for different Path
    */
    var method: Moya.Method {
        return .get
    }
    
    /**
    Defines sample data for testing
    */
    var sampleData: Data {
        switch self {
        default:
            let url = Bundle.main.url(forResource: "Schedule", withExtension: "json")!
            return try! Data(contentsOf: url)
        }
    }
    
    /**
    Defines headers for each api endpoint
    */
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
    
    /**
    Defines custom paramters for each api endpoint
    */
    var task:Task {
        switch self {
        case .schedule(let country, let date):
            return .requestParameters(parameters: ["country": country, "date": date], encoding: URLEncoding.default)
        case .searchShow(let showName):
            return .requestParameters(parameters: ["q": showName], encoding: URLEncoding.default)
        }
    }
}
