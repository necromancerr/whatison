//
//  Episode.swift
//  WhatIsOn
//
//  Created by Pratish Karmacharya on 5/1/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import Foundation

struct Show {
    let id : Int?
    let url : String?
    let name : String?
    let type : String?
    let genres : [String]?
    let status : String?
    let runtime : Int?
    let language : String?
    let premiered : String?
    let officialSite : String?
    let showImage : ShowImage?
    let network : Network?
    let summary : String?
    let score : Float?
}

extension Show : Decodable, Equatable {
    enum ShowCodingKeys: String, CodingKey {
        case id
        case url
        case name
        case type
        case status
        case language
        case premiered
        case showImage = "image"
        case network
        case summary
        case genres
        case runtime
        case officialSite
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ShowCodingKeys.self)
        
        id = try container.decodeIfPresent(Int.self, forKey: .id)!
        url = try container.decodeIfPresent(String.self, forKey: .url)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        type = try container.decodeIfPresent(String.self, forKey: .type)
        status = try container.decodeIfPresent(String.self, forKey: .status)
        language = try container.decodeIfPresent(String.self, forKey: .language)
        premiered = try container.decodeIfPresent(String.self, forKey: .premiered)
        showImage = try container.decodeIfPresent(ShowImage.self, forKey: .showImage)
        network = try container.decodeIfPresent(Network.self, forKey: .network)
        summary = try container.decodeIfPresent(String.self, forKey: .summary)
        genres = try container.decodeIfPresent([String].self, forKey: .genres)
        runtime = try container.decodeIfPresent(Int.self, forKey: .runtime)
        officialSite = try container.decodeIfPresent(String.self, forKey: .officialSite)
        score = nil
    }
    
    static func == (lhs: Show, rhs: Show) -> Bool {
        return lhs.id == rhs.id && lhs.url == rhs.url && lhs.name == rhs.name && lhs.type == rhs.type && lhs.status == rhs.status && lhs.language == rhs.language
    }
    
}
