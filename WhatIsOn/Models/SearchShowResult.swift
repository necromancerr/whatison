//
//  SearchShowResult.swift
//  WhatIsOn
//
//  Created by Pratish Karmacharya on 5/4/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import Foundation

struct SearchShowResult {
    let score: Float
    let show: Show
}

extension SearchShowResult: Decodable {
    enum SearchShowResultsCodingKeys: String, CodingKey {
        case score
        case show
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: SearchShowResultsCodingKeys.self)
        
        score = try container.decode(Float.self, forKey: .score)
        show = try container.decode(Show.self, forKey: .show)
    }
}
