//
//  Show.swift
//  WhatIsOn
//
//  Created by Pratish Karmacharya on 5/1/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import Foundation

struct Program {
    let id : Int?
    let url : String?
    let name : String?
    let season : Int?
    let number : Int?
    let runtime : Int?
    let airdate : String?
    let airtime : String?
    let airstamp : String?
    let summary : String?
    let score : Float?
    let show : Show?
}

extension Program: Decodable, Equatable {
    static func == (lhs: Program, rhs: Program) -> Bool {
        return lhs.name == rhs.name &&
            lhs.url == rhs.url &&
            lhs.id == rhs.id
            lhs.airdate == rhs.airdate &&
            lhs.airtime == rhs.airtime
        && lhs.airstamp == rhs.airstamp
        && lhs.number == rhs.number
        && lhs.show == rhs.show
    }
    
    enum ProgramCodingKeys: String, CodingKey {
        case id
        case url
        case name
        case season
        case number
        case runtime
        case airdate
        case airtime
        case airstamp
        case summary
        case show
        case score
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ProgramCodingKeys.self)
        
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        url = try container.decodeIfPresent(String.self, forKey: .url)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        season = try container.decodeIfPresent(Int.self, forKey: .season)
        number = try container.decodeIfPresent(Int.self, forKey: .number)
        runtime = try container.decodeIfPresent(Int.self, forKey: .runtime)
        airdate = try container.decodeIfPresent(String.self, forKey: .airdate)
        airtime = try container.decodeIfPresent(String.self, forKey: .airtime)
        airstamp = try container.decodeIfPresent(String.self, forKey: .airstamp)
        summary = try container.decodeIfPresent(String.self, forKey: .summary)
        show = try container.decodeIfPresent(Show.self, forKey: .show)
        score = try container.decodeIfPresent(Float.self, forKey: .score)
    }
}
