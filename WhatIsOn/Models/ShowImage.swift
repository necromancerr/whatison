//
//  ShowImage.swift
//  WhatIsOn
//
//  Created by Pratish Karmacharya on 5/1/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import Foundation

struct ShowImage {
    let medium : String?
    let original : String?
}

extension ShowImage : Decodable {
    enum ShowImageCodingKeys : String, CodingKey {
        case medium
        case original
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ShowImageCodingKeys.self)
        
        medium = try container.decodeIfPresent(String.self, forKey: .medium)
        original = try container.decodeIfPresent(String.self, forKey: .original)
    }
}

