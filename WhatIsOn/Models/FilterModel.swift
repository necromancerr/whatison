//
//  FilterModel.swift
//  WhatIsOn
//  Used to pass data between ProgramListViewController and ProgramFilterViewController
//  Created by Pratish Karmacharya on 5/2/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import Foundation

struct FilterModel {
    var date: String
    var country: String
    var showName: String?
    
    init(date: String, country: String) {
        self.date = date
        self.country = country
    }
}
