//
//  Network.swift
//  WhatIsOn
//
//  Created by Pratish Karmacharya on 5/3/19.
//  Copyright © 2019 Pratish Karmacharya. All rights reserved.
//

import Foundation

struct Network {
    let id : Int?
    let name : String?
}

extension Network : Decodable, Equatable {
    enum NetworkCodingKeys : String, CodingKey {
        case id
        case name
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: NetworkCodingKeys.self)
        
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        name = try container.decodeIfPresent(String.self, forKey: .name)
    }
    
    static func == (lhs: Network, rhs: Network) -> (Bool) {
        return lhs.id == rhs.id && lhs.name == rhs.name
    }
}
